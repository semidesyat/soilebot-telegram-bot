<img src="img/icon.jpg" alt="preview" width="300"/>

# soile - telegram bot
Бот голосовой помощник для преобразования аудиофайлов в текст.

## Описание

>Транскрибация (расшифровка) – это метод перевода информации из аудио или видео в текстовый формат. Такой подход актуален для слабослышащих, при расшифровке интервью и создании субтитров (для тех, кому нужно посмотреть видеоролик или прослушать аудио без звука). 

Цель транскрибации – перевести речь в текстовый формат, который будет понятен любому человеку. 

**Soilebot** - бот голосовой помощник для преобразования аудиофайлов в текст. Наш бот поможет избавить от боли прослушки сотни аудиосообщений.

## Preview

<img src="img/screen-2.PNG" alt="preview" width="200"/>
<img src="img/screen-1.PNG" alt="preview" width="200"/>

## Инструкция

1. Перейдите на страницу бота https://t.me/speechconverter_bot
2. Нажмите на "Start"
3. Запишите голосовое сообщение и отправьте боту
4. Наслаждайтесь полученным результатом!

## Техническое описание

Бот написан на языке Python.

Pytelegrambotapi 3.6.6 - Python Telegram bot api.

SpeechRecognition 3.8.1 - Library for performing speech recognition, with support for several engines and APIs, online and offline.

Сервер - https://www.pythonanywhere.com/